package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.model.DeckOfCards;
import edu.ntnu.idatt2003.model.HandOfCards;
import edu.ntnu.idatt2003.view.CardGameApp;

public class CardGameController {
  DeckOfCards deckOfCards;
  HandOfCards currentHand;
  CardGameApp view;
  public CardGameController(CardGameApp view) {
    deckOfCards = new DeckOfCards();
    currentHand = new HandOfCards();
    this.view = view;
  }

  public void dealHand(String input) {
    try {
      int n = Integer.parseInt(input);

      try {
        currentHand = deckOfCards.dealHand(n);
        view.displayDealtHand(currentHand);
      } catch (Exception e) {
        String errorMessage = "Make sure that the number you enter is between 1 and 52";
        view.displayErrorMessage(errorMessage);
      }

    } catch (Exception e) {
      String errorMessage = "Make sure to enter a number when dealing a hand";
      view.displayErrorMessage(errorMessage);
    }
  }

  public void checkHand() {
    if (currentHand.getCardsOnHand().isEmpty()) {
      view.displayErrorMessage("Cannot check your hand since you have not dealt a hand yet.");
    } else {
      String sum = String.valueOf(currentHand.checkSum());
      HandOfCards cardsOfHearts = currentHand.checkHearts();
      String queenOfSpades;
      String flush;

      if (currentHand.checkQueenOfSpades()) {
        queenOfSpades = "Yes";
      } else {
        queenOfSpades = "No";
      }

      if (currentHand.checkFlush()) {
        flush = "Yes";
      } else {
        flush = "No";
      }

      view.displayCheckHandInformation(sum, cardsOfHearts, queenOfSpades, flush);
    }
  }
}
