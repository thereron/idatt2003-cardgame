package edu.ntnu.idatt2003.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
  private final char[] suit = { 'S', 'H', 'D', 'C' };
  private final int[] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
  private final Random random = new Random();
  private final List<PlayingCard> deck;

  public DeckOfCards() {
    deck = new ArrayList<>();

    for(char suit : this.suit) {
      for (int face : this.face) {
        deck.add(new PlayingCard(suit, face));
      }
    }
  }

  public List<PlayingCard> getDeck() {
    return deck;
  }

  public HandOfCards dealHand(int n) {
    if (n < 1 || n > 52) {
      throw new IllegalArgumentException("The requested number of cards to deal is invalid");
    }
    HandOfCards hand = new HandOfCards();

    while (hand.getCardsOnHand().size() < n) {
      int randomCard = random.nextInt(deck.size() - 1);

      hand.addCard(deck.get(randomCard));
    }
    return hand;
  }

}
