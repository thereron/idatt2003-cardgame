package edu.ntnu.idatt2003;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2003.model.HandOfCards;
import edu.ntnu.idatt2003.model.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class TestHandOfCards {
  private HandOfCards handOfCards;

  @BeforeEach
  void setUp() {
    handOfCards = new HandOfCards();
  }

  @Test
  @DisplayName("Test constructor works with valid argument")
  void testConstructorWorksWithValidArgument() {
    assertNotNull(handOfCards);
  }

  @Test
  @DisplayName("Test addCard-method works with valid argument")
  void testAddCardWorksWithValidArgument() {
    PlayingCard playingCard = new PlayingCard('H', 2);

    handOfCards.addCard(playingCard);

    assertTrue(handOfCards.getCardsOnHand().get(0).equals(playingCard));
  }

  @Test
  @DisplayName("Test addCard-method throws with invalid argument")
  void testDealHandThrowsWithInvalidArgument() {
    PlayingCard invalidPlayingCard = null;

    assertThrows(IllegalArgumentException.class, () -> handOfCards.addCard(invalidPlayingCard));
  }

  @Test
  @DisplayName("Test CheckSum-method works with valid argument")
  void testCheckSumWorksWithValidArgument() {
    PlayingCard playingCard1 = new PlayingCard('H', 2);
    PlayingCard playingCard2 = new PlayingCard('H', 12);
    int realSum = 14;

    handOfCards.addCard(playingCard1);
    handOfCards.addCard(playingCard2);

    assertEquals(realSum, handOfCards.checkSum());
  }

  @Test
  @DisplayName("Test CheckHearts-method works with valid argument")
  void testCheckHeartWorksWithValidArgument() {
    PlayingCard playingCard1 = new PlayingCard('H', 2);
    PlayingCard playingCard2 = new PlayingCard('H', 12);
    int amountHearts = 2;

    handOfCards.addCard(playingCard1);
    handOfCards.addCard(playingCard2);

    assertEquals(amountHearts, handOfCards.checkHearts().getCardsOnHand().size());
  }

  @Test
  @DisplayName("Test CheckQueenOfSpades-method returns true with valid card")
  void testCheckQueenOfSpadesReturnsTrueWithValidCard() {
    PlayingCard playingCard1 = new PlayingCard('S', 12);

    handOfCards.addCard(playingCard1);

    assertTrue(handOfCards.checkQueenOfSpades());
  }

  @Test
  @DisplayName("Test CheckQueenOfSpades-method returns false with invalid card")
  void testCheckQueenOfSpadesReturnsFalseWithInvalidCard() {
    PlayingCard playingCard1 = new PlayingCard('H', 12);

    handOfCards.addCard(playingCard1);

    assertFalse(handOfCards.checkQueenOfSpades());
  }

  @Test
  @DisplayName("Test CheckFlush-method returns true with valid cards")
  void testCheckFlushReturnsTrueWithValidCards() {
    PlayingCard playingCard1 = new PlayingCard('S', 1);
    PlayingCard playingCard2 = new PlayingCard('S', 2);
    PlayingCard playingCard3 = new PlayingCard('S', 3);
    PlayingCard playingCard4 = new PlayingCard('S', 4);
    PlayingCard playingCard5 = new PlayingCard('S', 12);

    handOfCards.addCard(playingCard1);
    handOfCards.addCard(playingCard2);
    handOfCards.addCard(playingCard3);
    handOfCards.addCard(playingCard4);
    handOfCards.addCard(playingCard5);

    assertTrue(handOfCards.checkFlush());
  }

  @Test
  @DisplayName("Test CheckFlush-method returns false with invalid cards")
  void testCheckFlushReturnsFalseWithInvalidCards() {
    PlayingCard playingCard1 = new PlayingCard('S', 1);
    PlayingCard playingCard2 = new PlayingCard('S', 2);
    PlayingCard playingCard3 = new PlayingCard('S', 3);
    PlayingCard playingCard4 = new PlayingCard('S', 4);
    PlayingCard playingCard5 = new PlayingCard('H', 12);

    handOfCards.addCard(playingCard1);
    handOfCards.addCard(playingCard2);
    handOfCards.addCard(playingCard3);
    handOfCards.addCard(playingCard4);
    handOfCards.addCard(playingCard5);

    assertFalse(handOfCards.checkFlush());
  }
}
