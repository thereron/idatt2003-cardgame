package edu.ntnu.idatt2003;


import edu.ntnu.idatt2003.model.PlayingCard;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestPlayingCard {

  private char VALIDSUIT = 'H';
  private int VALIDFACE = 2;
  private char INVALIDSUIT = 'A';
  private int INVALIDFACE = 0;

  @Test
  @DisplayName("Test constructor works with valid argument")
  void testConstructorWorksWithValidArgument() {
    PlayingCard playingCard = new PlayingCard(VALIDSUIT, VALIDFACE);

    assertEquals(VALIDSUIT, playingCard.getSuit());
    assertEquals(VALIDFACE, playingCard.getFace());
  }

  @Test
  @DisplayName("Test constructor throws with invalid argument")
  void testConstructorThrowsWithInvalidArgument() {
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard(INVALIDSUIT, VALIDFACE));
    assertThrows(IllegalArgumentException.class, () -> new PlayingCard(INVALIDSUIT, INVALIDFACE));
  }
}
