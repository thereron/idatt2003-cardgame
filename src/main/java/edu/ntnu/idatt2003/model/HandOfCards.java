package edu.ntnu.idatt2003.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HandOfCards {
  private final List<PlayingCard> cardsOnHand;

  public HandOfCards() {
    cardsOnHand = new ArrayList<>();
  }
  public HandOfCards(List<PlayingCard> handOfCards) {
    this.cardsOnHand = handOfCards;
  }

  public List<PlayingCard> getCardsOnHand() {
    return cardsOnHand;
  }

  public void addCard(PlayingCard card) {
    if (card == null) {
      throw new IllegalArgumentException("There was an attempt at using a null-value as an " +
          "argument");
    }

    if (!cardsOnHand.contains(card)) {
      cardsOnHand.add(card);
    }
  }

  //hjelp fra chat med å gjøre det med streams, brukte for-loop først
  public int checkSum() {
    return cardsOnHand.stream()
        .mapToInt(PlayingCard::getFace) //spørre chat om hvordan denne funker
        .sum();
  }

  public HandOfCards checkHearts() {
    return new HandOfCards(cardsOnHand.stream()
        .filter(card -> card.getSuit() == 'H')
        .toList());
  }

  //gjorde først med for-loop, spurte så chat om hvordan man gjør det med streams
  public boolean checkQueenOfSpades() {
    return cardsOnHand.stream()
        .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
  }

  //hjelp fra chat med å gjøre det med streams
  public boolean checkFlush() {
    Map<Character, Long> suitAmount = cardsOnHand.stream()
        .collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting()));

    return suitAmount.values().stream().anyMatch(amount -> amount >= 5);
  }

}
