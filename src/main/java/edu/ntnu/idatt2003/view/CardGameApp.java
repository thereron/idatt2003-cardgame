package edu.ntnu.idatt2003.view;

import edu.ntnu.idatt2003.controller.CardGameController;
import edu.ntnu.idatt2003.model.HandOfCards;
import edu.ntnu.idatt2003.model.PlayingCard;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.text.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.*;


public class CardGameApp extends Application {
  int BOX_WIDTH = 200;

  CardGameController cardGameController;
  VBox leftBox;
  VBox rightBox;
  VBox cardShowingBox;
  Text textDisplayOfCards;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    HBox root = new HBox();

    leftBox = createLeftBox();
    leftBox.setAlignment(Pos.CENTER);
    rightBox = createRightBox();
    rightBox.setAlignment(Pos.CENTER);

    root.setSpacing(50);
    root.getChildren().addAll(leftBox, rightBox);

    //root.setBackground(Background.fill(Paint.valueOf("012931")));
    //leftBox.setBackground(Background.fill(Paint.valueOf("FFFFFF")));
    //rightBox.setBackground(Background.fill(Paint.valueOf("FFFFFF")));

    Scene scene = new Scene(root, 700, 450);

    root.setAlignment(Pos.CENTER);


    primaryStage.setTitle("Card game");
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  @Override
  public void init() throws Exception {
    super.init();

    cardGameController = new CardGameController(this);
    leftBox = new VBox();
    rightBox = new VBox();
    cardShowingBox = new VBox();
    textDisplayOfCards = new Text();
  }

  @Override
  public void stop() throws Exception {
    super.stop();
  }

  private VBox createLeftBox() {
    VBox leftBox = new VBox(25);
    leftBox.setAlignment(Pos.CENTER);

    cardShowingBox = new VBox();

    textDisplayOfCards = new Text("Cards will be displayed here");

    textDisplayOfCards.setTextAlignment(TextAlignment.CENTER);
    textDisplayOfCards.setWrappingWidth(BOX_WIDTH);
    cardShowingBox.getChildren().add(textDisplayOfCards);

    HBox cardButtonBox = new HBox();
    Button button1 = new Button("Deal hand");
    Button button2 = new Button("Check hand");

    button1.setOnAction(actionEvent -> displayInputRequest());
    button2.setOnAction(actionEvent -> cardGameController.checkHand());

    cardButtonBox.setSpacing(20);
    cardButtonBox.setAlignment(Pos.CENTER);
    cardButtonBox.getChildren().addAll(button1, button2);

    leftBox.setMinWidth(BOX_WIDTH);
    leftBox.setMaxWidth(BOX_WIDTH);
    leftBox.getChildren().addAll(cardShowingBox, cardButtonBox);

    return leftBox;
  }

  private VBox createRightBox() {
    VBox rightBox = new VBox(25);
    rightBox.setAlignment(Pos.CENTER);

    Text text1 = new Text("Press the deal hand method to the left, " +
        "and check hand afterwards to view the statistics\n");
    Text text2 = new Text("<--");

    text1.setWrappingWidth(BOX_WIDTH);
    text2.setWrappingWidth(BOX_WIDTH);

    text1.setTextAlignment(TextAlignment.CENTER);
    text2.setTextAlignment(TextAlignment.CENTER);

    rightBox.setMinWidth(BOX_WIDTH);
    rightBox.setMaxWidth(BOX_WIDTH);
    rightBox.getChildren().addAll(text1, text2);

    return rightBox;
  }

  public void displayInputRequest() {
    Stage popupWindow = new Stage();
    popupWindow.initModality(Modality.APPLICATION_MODAL);

    Text information = new Text("Please enter the amount of cards you wish to deal");
    TextField inpAmountOfCards =
        new TextField();
    inpAmountOfCards.setMaxWidth(50);
    inpAmountOfCards.setAlignment(Pos.CENTER);

    Button btnDeal = new Button("Deal");
    btnDeal.setBorder(Border.EMPTY);
    btnDeal.setBackground(Background.fill(Paint.valueOf("3EB489")));
    btnDeal.setShape(new Circle(2));
    btnDeal.setOnMouseEntered(actionEvent -> btnDeal.setBackground(Background.fill(Paint.valueOf("GREEN"))));
    btnDeal.setOnMouseExited(actionEvent -> btnDeal.setBackground(Background.fill(Paint.valueOf("3EB489"))));

    Button btnExit = new Button("Exit");
    btnExit.setBorder(Border.EMPTY);
    btnExit.setBackground(Background.fill(Paint.valueOf("FF7F7F")));
    btnExit.setShape(new Circle(2));
    btnExit.setOnMouseEntered(actionEvent -> btnExit.setBackground(Background.fill(Paint.valueOf("RED"))));
    btnExit.setOnMouseExited(actionEvent -> btnExit.setBackground(Background.fill(Paint.valueOf("FF7F7F"))));

    HBox buttonContainer = new HBox(btnDeal, btnExit);
    buttonContainer.setAlignment(Pos.CENTER);

    btnDeal.setOnAction(actionEvent -> {
      cardGameController.dealHand(inpAmountOfCards.getText());
      popupWindow.close();
    });
    btnExit.setOnAction(actionEvent -> popupWindow.close());

    VBox box = new VBox(10);
    box.getChildren().addAll(information, inpAmountOfCards, buttonContainer);
    box.setAlignment(Pos.CENTER);

    popupWindow.setScene(new Scene(box, 350, 150));
    popupWindow.showAndWait();
  }

  public void displayErrorMessage(String errorMessage) {
    Alert alert = new Alert(AlertType.ERROR);
    alert.setTitle("Error Dialog");
    alert.setHeaderText("An error occurred!");
    alert.setContentText(errorMessage);
    alert.showAndWait();
  }

  public void displayDealtHand(HandOfCards hand) {
    String stringOfCards = turnHandOfCardsToString(hand);
    textDisplayOfCards.setText(stringOfCards);
  }

  public void displayCheckHandInformation(String sum, HandOfCards cardsOfHearts, String queenOfSpades, String flush) {
    rightBox.getChildren().clear();

    Label label = new Label("Statistics");

    GridPane statistics = new GridPane();

    Text sumText = new Text("Sum of the faces: ");
    Text cardOfHeartsText = new Text("Cards of hearts: ");
    Text queenOfspadesText = new Text("Queen of spades: ");
    Text flushText = new Text("Flush: ");

    Text textOfHearts;
    if (turnHandOfCardsToString(cardsOfHearts).isBlank()) {
      textOfHearts = new Text("No hearts");
    } else {
      textOfHearts = new Text(turnHandOfCardsToString(cardsOfHearts));
    }

    statistics.add(sumText, 0, 0);
    statistics.add(new Text(sum), 1, 0);

    statistics.add(cardOfHeartsText, 0, 1);
    statistics.add(textOfHearts, 1, 1);

    statistics.add(queenOfspadesText, 0, 2);
    statistics.add(new Text(queenOfSpades), 1, 2);

    statistics.add(flushText, 0, 3);
    statistics.add(new Text(flush), 1, 3);

    statistics.getChildren().forEach(child -> {
      if (child instanceof Text) {
        ((Text) child).setWrappingWidth((double) BOX_WIDTH/2);
      }
    });

    statistics.setHgap(20);
    statistics.setVgap(20);

    rightBox.getChildren().addAll(label, statistics);
  }

  public String turnHandOfCardsToString(HandOfCards hand) {
    StringBuilder stringOfCards = new StringBuilder();
    hand.getCardsOnHand().forEach(card -> stringOfCards.append(printCard(card)).append("  "));

    return stringOfCards.toString();
  }

  public String printCard(PlayingCard card) {
    return card.getSuit() + "" + card.getFace();
  }
}
