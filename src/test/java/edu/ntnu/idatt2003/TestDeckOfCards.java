package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.model.DeckOfCards;
import edu.ntnu.idatt2003.model.HandOfCards;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestDeckOfCards {

  private DeckOfCards deckOfCards;

  @BeforeEach
  void setUp() {
    deckOfCards = new DeckOfCards();
  }

  @Test
  @DisplayName("Test constructor works with valid argument")
  void testConstructorWorksWithValidArgument() {
    assertNotNull(deckOfCards);
    assertFalse(deckOfCards.getDeck().stream().anyMatch(Objects::isNull));
  }

  //hjelp fra chat
  @Test
  @DisplayName("Test dealHand-method works with valid argument")
  void testDealHandWorksWithValidArgument() {
    HandOfCards hand = deckOfCards.dealHand(5);

    assertTrue(hand.getCardsOnHand().stream().allMatch(card ->
        hand.getCardsOnHand().indexOf(card) == hand.getCardsOnHand().lastIndexOf(card)));
  }

  @Test
  @DisplayName("Test dealHand-method throws with invalid argument")
  void testDealHandThrowsWithInvalidArgument() {
    int invalidArgument1 = 0;
    int invalidArgument2 = 53;

    assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(invalidArgument1));
    assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(invalidArgument2));
  }
}
